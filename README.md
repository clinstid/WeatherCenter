# WeatherCenter supybot plugin
A weather plugin for supybot that uses [OpenWeatherMap](https://openweathermap.org/api) and [Google Geocoding](https://developers.google.com/maps/documentation/geocoding/start) APIs. Set the following configuration parameters in supybot to use the plugin:

```
config supybot.plugins.WeatherCenter.googleAPIKey <google-api-key>
config supybot.plugins.WeatherCenter.openWeatherMapAppId <openweathermap-app-id>
```
