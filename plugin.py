# -*- coding: utf-8 -*-
from supybot.commands import *
import supybot.plugins as plugins
import sys
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
from types import *
from string import lstrip
from datetime import datetime, timedelta
from pytz import timezone, utc
from timezonefinder import TimezoneFinder
import time

import requests

DEGREE_SYMBOL = '°'

class Location:
    location_cache = {}

    def __init__(self):
        self.zip_code = None
        self.q = None
        self.lat = None
        self.lon = None
        self.name = None

    @classmethod
    def get_location_id(cls, gapi_key, location):
        if gapi_key is None:
            raise Exception('gapi_key cannot be None!')

        if location in cls.location_cache:
            return cls.location_cache[location]

        # Try figuring stuff out using google's geocoding
        url = 'https://maps.googleapis.com/maps/api/geocode/json'
        response = requests.get(
            url,
            params={
                'address': location,
                'key': gapi_key
            }
        )

        if response.status_code == 200:
            body = response.json()
            results = body.get('results')
            if results is not None and len(results) > 0:
                location_response = Location()
                result = results[0]
                location_response.name = result.get('formatted_address', location)
                geometry = result.get('geometry')
                if geometry is not None and 'location' in geometry:
                    location_response.lat = geometry['location']['lat']
                    location_response.lon = geometry['location']['lng']
                    # We only return from here if we got all of the location
                    # identification data. If we didn't get lat/lng, then we'll
                    # fall back to the raw location (or zip code) and hope that
                    # OpenWeatherMap can figure it out.
                    cls.location_cache[location] = location_response
                    return location_response

        # If the geocoding failed, then attempt a zip code, otherwise fall all the
        # way back to just shoving the location string at the weather API
        try:
            zip_code = int(location)
        except ValueError:
            location_response.q = location
        else:
            location_response.zip_code = location

        cls.location_cache[location] = location_response
        return location_response

    def get_tz_for_geo(self):
        tf = TimezoneFinder()
        return timezone(tf.timezone_at(lng=self.lon, lat=self.lat))

    def get_query_param(self):
        query_params = None
        if self.lat is not None and self.lon is not None:
            query_params = {
                'lat': self.lat,
                'lon': self.lon
            }
        elif self.zip_code is not None:
            query_params = {
                'zip': self.zip_code
            }
        else:
            query_params = {
                'q': self.q
            }
        return query_params


class OpenWeatherMapClient:
    BASE_WEATHER_URL = 'https://api.openweathermap.org/data/2.5/{}'

    def __init__(self, appid):
        if appid is None:
            raise Exception('appid cannot be None!')

        self.appid = appid

    def get_current_weather_for_location(self, location_qp):
        query_params = {
            'appid': self.appid,
            'units': 'imperial',
        }

        query_params.update(location_qp)

        response = requests.get(self.BASE_WEATHER_URL.format('weather'), params=query_params)
        response.raise_for_status()
        return response.json()

    def get_forecast_for_location(self, location_qp):
        query_params = {
            'appid': self.appid,
            'units': 'imperial',
        }

        query_params.update(location_qp)

        response = requests.get(self.BASE_WEATHER_URL.format('forecast'), params=query_params)
        response.raise_for_status()
        return response.json()

class ForecastPoint:
    def __init__(self, dt, temp, humidity, description):
        self.dt = dt
        self.temp = temp
        self.humidity = humidity
        self.description = description

def process_24_hour_forecast(data_points):
    temps = []
    now = datetime.utcnow()
    tomorrow = now + timedelta(hours=24)
    forecast_points = []
    for data_point in data_points:
        dt = datetime.fromtimestamp(data_point['dt'])
        if dt < now:
            continue
        if dt >= tomorrow:
            break

        main = data_point.get('main')
        temp = main.get('temp')
        temps.append(temp)

        weather = data_point.get('weather')[0]
        forecast_points.append(
            ForecastPoint(dt, temp, main.get('humidity'),
                          weather.get('description'))
        )

    return {
        'min': min(temps),
        'max': max(temps),
        'avg': sum(temps) / float(len(temps)),
        'forecasts': forecast_points,
    }

def get_wind_dir(deg):
    """
    45: NE
    90: E
    135: SE
    180: S
    225: SW
    270: W
    315: NW
    360: N
    """
    if deg <= 20 or deg >= 340:
        return 'N'
    elif deg <= 60:
        return 'NE'
    elif deg <= 110:
        return 'E'
    elif deg <= 160:
        return 'SE'
    elif deg <= 200:
        return 'S'
    elif deg <= 240:
        return 'SW'
    elif deg <= 290:
        return 'W'
    elif deg <= 340:
        return 'NW'

class WeatherCenter(callbacks.Plugin):
    weather_cache = {}

    @classmethod
    def get_cached_location(cls, location):
        return cls.weather_cache.get(location)

    def weather(self, irc, msg, args, location):
        """<location>

        Print weather information for the specified <location> from
        OpenWeatherMap
        """
        appid = self.registryValue('openWeatherMapAppId')
        gapi_key = self.registryValue('googleAPIKey')

        if appid is None:
            irc.reply('Set supybot.plugins.WeatherCenter.openWeatherMapAppId to your OpenWeatherMap App ID')
            return

        if gapi_key is None:
            irc.reply('Set supybot.plugins.WeatherCenter.googleAPIKey to your Google Geocoding API key')
            return

        locationstr = ' '.join(location)
        locationstr = locationstr.lstrip().rstrip()

        location_id = Location.get_location_id(gapi_key, locationstr)
        location_qp = location_id.get_query_param()
        location_tz = location_id.get_tz_for_geo()

        now = datetime.utcnow()
        ten_minutes_ago = now - timedelta(minutes=10)

        current_weather = None
        forecast = None
        cached_weather = self.get_cached_location(locationstr)
        if cached_weather is not None:
            self.log.info('Loading weather from cache')
            if cached_weather['dt'] > ten_minutes_ago:
                current_weather = cached_weather['current_weather']
                forecast = cached_weather['forecast']
            else:
                WeatherCenter.weather_cache.pop(locationstr, None)

        if current_weather is None or forecast is None:
            weather_client = OpenWeatherMapClient(appid)

            try:
                current_weather = weather_client.get_current_weather_for_location(location_qp)
            except requests.HTTPError as ex:
                self.log.error(str(ex))
                if ex.response.status_code == 404:
                    reason = 'location not found'
                else:
                    reason = 'internal error'
                irc.reply('Failed to load current weather: {}'.format(reason))
                return

            try:
                forecast = weather_client.get_forecast_for_location(location_qp)
            except requests.HTTPError as ex:
                self.log.error(str(ex))
                if ex.response.status_code == 404:
                    reason = 'location not found'
                else:
                    reason = 'internal error'
                irc.reply('Failed to load forecast: {}'.format(reason))
                return

            WeatherCenter.weather_cache[str(locationstr)] = {
                'dt': now,
                'current_weather': current_weather,
                'forecast': forecast,
            }

        forecast_data = process_24_hour_forecast(forecast.get('list'))

        main = current_weather['main']
        reply = 'Currently at {loc} the temperature is {temp}{deg}F. Humidity: {humidity}%.'.format(
            loc=location_id.name,
            temp=main['temp'],
            deg=DEGREE_SYMBOL,
            humidity=main['humidity'],
        )

        wind = current_weather['wind']
        if 'deg' in wind:
            wind_str = ' Wind is {wind_speed}mph {wind_dir} {wind_deg}{deg}.'.format(
                deg=DEGREE_SYMBOL,
                wind_dir=get_wind_dir(wind['deg']),
                wind_deg=wind['deg'],
                wind_speed=wind['speed']
            )
        else:
            wind_str = ' Wind is {wind_speed}mph.'.format(wind_speed=wind['speed'])

        reply += wind_str
        irc.reply(reply)

        reply = ('Next 24 hours: High {high}{deg}F Low {low}{deg}F / '.format(
            deg=DEGREE_SYMBOL,
            high=forecast_data['max'],
            low=forecast_data['min'],
        ))
        replies = []
        for forecast in forecast_data['forecasts']:
            replies.append(
                '{time}: {temp:0.2f}{deg}F {humidity:2.0f}% {description}'.format(
                    time=utc.localize(forecast.dt).astimezone(location_tz).strftime('%I:%M %p'),
                    temp=forecast.temp,
                    deg=DEGREE_SYMBOL,
                    humidity=forecast.humidity,
                    description=forecast.description,
                )
            )
        reply += ' / '.join(replies)
        irc.reply(reply)


    weather = wrap(weather, [many('something')])

Class = WeatherCenter
# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
